# portfolio
This website acts as my personal portfolio.

## Updating
The process for getting updates into Codeberg pages is as follows: 

1. checkout a new branch from main with the update
2. apply the updates to the new branch
3. create a merge request 
4. merge into `main` 
5. checkout `pages` and pull `main` into `pages`

## Hosting 
This code is hosted on codeberg pages. 

## Materialize CSS 
[Materialize CSS](https://materializecss.com/) licensed under the [MIT License](https://github.com/Dogfalo/materialize/blob/v1-dev/LICENSE). 